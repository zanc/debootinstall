#!/bin/sh
# vim: se ts=2 sw=2 et:

target=/mnt/debian
deb_repo=$(cd ../debrepo && pwd)
lists=$(cd ../lists && pwd)
arch=amd64
suite=stretch
hostname=zanc
timezone=Asia/Jakarta
commit_user='Azure Zanculmarktum'
commit_email='zanculmarktum@gmail.com'
firmwares="iwlwifi"
packages="\
console-setup \
vim-gtk \
wpasupplicant \
wireless-tools \
gpm \
policykit-1 \
xserver-xorg-video-dummy xserver-xorg-input-void xserver-xorg-core xinit x11-xserver-utils \
xserver-xorg-video-intel \
xserver-xorg-input-libinput \
libgl1-mesa-dri mesa-utils \
xinput \
rxvt-unicode \
alsa-utils \
tlp \
htop \
reprepro \
curl \
linux-headers-amd64"
mirror=http://kartolo.sby.datautama.net.id/debian/

# Prepends sbin to $PATH if ran with sudo
if [ -n "$SUDO_USER" ]; then
  export PATH=/usr/local/sbin:/usr/sbin:/sbin:$PATH
fi

my_chroot() {
  LANG=C.UTF-8 TERM=xterm chroot $target /bin/bash -l "$@"
}

etckeeper_commit() {
  my_chroot <<EOF
cd /etc
git add $1
git commit -m '$2'
EOF
}

set -e

# Sanity checks
if [ "$(id -u)" != "0" ]; then
  echo "$0: superuser access required" >&2
  exit 1
fi

if [ ! -d "$target" ]; then
  echo "$0: \$target: invalid target" >&2
  exit 1
fi

if [ ! -d "$deb_repo/dists" -o ! -d "$deb_repo/pool" ]; then
  echo "$0: \$deb_repo: invalid repository" >&2
  exit 1
fi

disk=$(mount | grep -Fe "on $target type" | cut -d' ' -f1)
if [ -z "$disk" ]; then
  echo "$0: \$target: not mounted" >&2
  exit 1
fi

# Bootstrap Debian
/usr/sbin/debootstrap --arch=$arch --variant=minbase --no-check-gpg $suite $target file://$deb_repo

# Get the UUID
uuid=$(/sbin/blkid $disk | sed -ne 's/.* UUID="\([^"]*\)".*/\1/p')

# Create fstab
>$target/etc/fstab
cat <<EOF >>$target/etc/fstab
# /etc/fstab: static file system information.
#
# file system    mount point   type    options                  dump pass
# $disk
UUID=$uuid / ext4 defaults 0 1
EOF
if [ -f "$target/swapfile" ]; then
  cat <<EOF >>$target/etc/fstab
/swapfile none swap defaults 0 0
EOF
fi

# Set the hardware clock
cat <<EOF >$target/etc/adjtime
0.0 0 0.0
0
LOCAL
EOF

# Set the timezone
echo $timezone >$target/etc/timezone
ln -sf /usr/share/zoneinfo/$timezone $target/etc/localtime

# Set the hostname
echo "$hostname" >$target/etc/hostname

# Create hosts
cat <<EOF >$target/etc/hosts
127.0.0.1 localhost
127.0.1.1 $hostname.example.org $hostname

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

# Configure Apt
cat <<EOF >$target/etc/apt/sources.list
# 

# deb cdrom:[Debian GNU/Linux 9.6.0 _Stretch_ - Official amd64 NETINST 20181110-11:34]/ stretch contrib main non-free

#deb cdrom:[Debian GNU/Linux 9.6.0 _Stretch_ - Official amd64 NETINST 20181110-11:34]/ stretch contrib main non-free

deb $mirror stretch main non-free contrib
deb-src $mirror stretch main non-free contrib

deb http://security.debian.org/debian-security stretch/updates main contrib non-free
deb-src http://security.debian.org/debian-security stretch/updates main contrib non-free

# stretch-updates, previously known as 'volatile'
deb $mirror stretch-updates main contrib non-free
deb-src $mirror stretch-updates main contrib non-free

# stretch-backports, previously on backports.debian.org
deb $mirror stretch-backports main contrib non-free
deb-src $mirror stretch-backports main contrib non-free
EOF
cp $lists/* $target/var/lib/apt/lists
find $deb_repo/pool -type f -name '*.deb' -exec cp {} $target/var/cache/apt/archives \;

# Create dev and the likes
mount -t devtmpfs devtmpfs $target/dev
mount -t devpts devpts $target/dev/pts
mount -t sysfs sysfs $target/sys
mount -t proc proc $target/proc

# Set the date from hardware
my_chroot <<EOF
hwclock --hctosys
EOF

# Install standard packages
my_chroot <<EOF
apt-get -o APT::Install-Recommends=0 -y install aptitude
aptitude -o APT::Install-Recommends=0 -y install ~pstandard ~prequired ~pimportant
EOF

# Install apt-file
my_chroot <<EOF
apt-get -o APT::Install-Recommends=0 -y install apt-file
EOF

# Install firmwares
my_chroot <<EOF
for fw in $firmwares; do
  apt-get -o APT::Install-Recommends=0 -y install firmware-\$fw
done
EOF

# Install kernel
my_chroot <<EOF
apt-get -o APT::Install-Recommends=0 -y install linux-image-$arch
EOF

# Install lilo
my_chroot <<EOF
DEBIAN_FRONTEND=noninteractive apt-get -o APT::Install-Recommends=0 -y install lilo
EOF

# Install sudo
my_chroot <<EOF
apt-get -o APT::Install-Recommends=0 -y install sudo
EOF

# Install additional packages
my_chroot <<EOF
DEBIAN_FRONTEND=noninteractive apt-get -o APT::Install-Recommends=0 -y install $packages
EOF

# Install git
my_chroot <<EOF
apt-get -y install git
git config --global user.name "$commit_user"
git config --global user.email "$commit_email"
EOF

# Install etckeeper
my_chroot <<EOF
apt-get -y install etckeeper
EOF
sed -i -e '/^#AVOID_DAILY_AUTOCOMMITS=/s/^#//' $target/etc/etckeeper/etckeeper.conf
etckeeper_commit etckeeper/etckeeper.conf 'etckeeper: disable daily autocommit'

echo "" >>$target/etc/.gitignore
# Exclude shadow from etckeeper
cat <<EOF >>$target/etc/.gitignore
/shadow*
/gshadow*
EOF
my_chroot <<EOF
cd /etc
git rm --cached shadow* gshadow*
EOF
etckeeper_commit .gitignore 'exclude shadow'

# Exclude resolv.conf from etckeeper
cat <<EOF >>$target/etc/.gitignore
/resolv.conf
EOF
my_chroot <<EOF
cd /etc
git rm --cached resolv.conf
EOF
etckeeper_commit .gitignore 'exclude resolv.conf'

# Setup sudoers
sed -i -e '/^Defaults	secure_path/s/.*/&\nDefaults	targetpw/' $target/etc/sudoers
etckeeper_commit sudoers 'sudo: ask for the root password'
sed -i -e '/^Defaults	targetpw/s/.*/&\nDefaults	timestamp_timeout=0/' $target/etc/sudoers
etckeeper_commit sudoers 'sudo: always ask for the password'

# Setup interfaces
cat <<EOF >$target/etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback
EOF
etckeeper_commit network/interfaces 'interfaces: set default'
cat <<EOF >>$target/etc/network/interfaces

# The primary network interface
allow-hotplug wlp3s0
iface wlp3s0 inet dhcp
	wpa-ssid name
	wpa-psk  pass
EOF
etckeeper_commit network/interfaces 'interfaces: add wlp3s0'

# Set the console font
if [ ! -f "$target/etc/console-setup/cached_Lat15-Terminus12x6.psf.gz" ]; then
  cp -p $target/usr/share/consolefonts/Lat15-Terminus12x6.psf.gz \
        $target/etc/console-setup/cached_Lat15-Terminus12x6.psf.gz
fi
sed -i \
  -e '\,setfont '"'"'.*,s,,setfont '"'"'/etc/console-setup/cached_Lat15-Terminus12x6.psf.gz'"'"' ,' \
  $target/etc/console-setup/cached_setup_font.sh
sed -i \
  -e '\,loadkeys '"'[^']*'"' > '"'"'/dev/null'"'"'.*,s,,loadkeys '"'"'/etc/console-setup/cached_UTF-8_del.kmap.gz'"'"' > '"'"'/dev/null'"'"' ,' \
  $target/etc/console-setup/cached_setup_keyboard.sh
sed -i \
  -e '/^CODESET=/s/.*/CODESET="Lat15"/' \
  -e '/^FONTFACE=/s/.*/FONTFACE="Terminus"/' \
  -e '/^FONTSIZE=/s/.*/FONTSIZE="6x12"/' \
  $target/etc/default/console-setup
etckeeper_commit "\
  console-setup/cached_Lat15-Terminus12x6.psf.gz \
  console-setup/cached_setup_font.sh \
  console-setup/cached_setup_keyboard.sh \
  default/console-setup" \
  'console-setup: set to Terminus'

# Prevent suspending when the lid is closed
echo '' >>$target/etc/systemd/logind.conf
echo 'HandleLidSwitch=ignore' >>$target/etc/systemd/logind.conf
etckeeper_commit systemd/logind.conf 'systemd-logind: prevent suspending when the lid is closed'

# Prevent systemd-tmpfiles from deleting /tmp
mkdir -p $target/etc/tmpfiles.d
cp -p $target/usr/lib/tmpfiles.d/tmp.conf $target/etc/tmpfiles.d/
etckeeper_commit tmpfiles.d/tmp.conf 'systemd-tmpfiles: add tmpfiles.d/tmp.conf'

sed -i -e '\,^D /tmp,s,,d /tmp,' $target/etc/tmpfiles.d/tmp.conf
etckeeper_commit tmpfiles.d/tmp.conf 'systemd-tmpfiles: prevent from deleting /tmp'

# Prevent apt from clearing cache
echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' >$target/etc/apt/apt.conf.d/01keep-debs
etckeeper_commit apt/apt.conf.d/01keep-debs 'apt: prevent from clearing cache'
